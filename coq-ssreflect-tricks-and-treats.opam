# This file was generated from `meta.yml`, please do not edit manually.
# Follow the instructions on https://gitlab.com/ana-borges/templates to regenerate.

opam-version: "2.0"
maintainer: "juanconrod"
version: "dev"

homepage: "https://gitlab.com/JuanCoRo/ssreflect-tricks-and-treats"
dev-repo: "git+https://gitlab.com/JuanCoRo/ssreflect-tricks-and-treats.git"
bug-reports: "https://gitlab.com/JuanCoRo/ssreflect-tricks-and-treats/issues"
doc: "https://JuanCoRo.gitlab.io/ssreflect-tricks-and-treats/"
license: "Unlicense"

synopsis: "Tricks and Treats of the SSReflect proof language and the Mathematical Components library"
description: """
This is a summary of all the nice tricks and treats I have been gathering about the [SSReflect](https://coq.inria.fr/refman/proof-engine/ssreflect-proof-language.html) proof language and the [MathComp](https://math-comp.github.io/) library in my years of proving."""

build: [make "-j%{jobs}%" ]
install: [make "install"]
depends: [
  "coq" {(>= "8.12" & < "8.14~")}
  "coq-mathcomp-ssreflect" {(>= "1.12" & < "1.13~")}
]

tags: [
  "category:Manuals"
  "keyword:ssreflect"
  "keyword:tricks"
  "keyword:treats"
  "keyword:coq formalization"
  "logpath:ssreflect_t_and_t"
]
authors: [
  "Juan Conejero"
]
