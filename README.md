<!---
This file was generated from `meta.yml`, please do not edit manually.
Follow the instructions on https://gitlab.com/ana-borges/templates to regenerate.
--->
# SSReflect tricks and treats

[![GitLab CI][pipeline-shield]][pipeline-link]
[![coqdoc][coqdoc-shield]][coqdoc-link]

[pipeline-shield]: https://img.shields.io/gitlab/pipeline/JuanCoRo/ssreflect-tricks-and-treats/main
[pipeline-link]: https://gitlab.com/JuanCoRo/ssreflect-tricks-and-treats/-/pipelines


[coqdoc-shield]: https://img.shields.io/badge/docs-coqdoc-blue.svg
[coqdoc-link]: https://JuanCoRo.gitlab.io/ssreflect-tricks-and-treats/ssreflect_t_and_t.ssrtt.html


This is a summary of all the nice tricks and treats I have been gathering about the [SSReflect](https://coq.inria.fr/refman/proof-engine/ssreflect-proof-language.html) proof language and the [MathComp](https://math-comp.github.io/) library in my years of proving.

## Meta

- Author(s):
  - Juan Conejero (initial)
- License: [Unlicense](LICENSE)
- Compatible Coq versions: 8.12 and 8.13
- Additional dependencies:
  - [MathComp](https://math-comp.github.io) 1.12.0 (`ssreflect` suffices)
- Coq namespace: `ssreflect_t_and_t`
- Related publication(s): none

## Building and installation instructions

Start by making sure the dependencies are installed. Then:
``` shell
git clone https://gitlab.com/JuanCoRo/ssreflect-tricks-and-treats.git
cd ssreflect-tricks-and-treats
make   # or make -j <number-of-cores-on-your-machine>
```


## HTML Documentation

To generate HTML documentation, run `make coqdoc` and point your browser at
`docs/coqdoc/toc.html`.

The documentation is also available
[online](https://JuanCoRo.gitlab.io/sreflect-tricks-and-treats/toc.html).
 
The documentation is generated with
[CoqdocJS](https://github.com/palmskog/coqdocjs).

## File Contents

- `ssrtt.v`: tricks and treats of ssreflect
