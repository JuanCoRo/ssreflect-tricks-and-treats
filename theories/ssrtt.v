Set Warnings "-notation-overridden, -ambiguous-paths".
From mathcomp Require Import all_ssreflect.

(*** Managing the context *)

(** Playing with implicatoins *)

Section implications.

Variables A B C : Type.
Variable P Q: Type -> Prop.
Variables n m p: nat.

(** [move=> /[swap].] *)
(* In the following goal, by the hypothesis [A -> B], the goal follows.        *)
(* However, since [C] is in the middle, we can either assume [A] and          *)
(* apply [A -> B], or change the order of the implication with [move=> /[swap]] *)
(* and let Coq solve the goal automatically                                   *)
Goal (A -> B) -> A -> C -> B.
Proof.
  move=> imp_AB.
  (* See that indeed the goal cannot be solved automatically *)
  Fail by [].
  move=> /[swap].
  by [].
Qed.

(** Directly applying a hypothesis in the stack *)
(* As discussed in the previous example, if we wanted to apply [A -> B], it is *)
(* too bureaucratic to name the hypothesis just so that we can apply it.      *)
(* In cases like this, [move=> /[apply]] lets us directly apply the topmost    *)
(* assumption of the stack to the inmediate next one.                         *)
Goal (A -> B) -> A -> C -> B.
Proof.
  (* Unless we first apply [A -> B] to A, Coq won't solve this by itself *)
  Fail by [].
  move=> /[apply].
  by [].
Qed.

(** [move=> /[dup].] *)
(* If a hypothesis can be used by two different views, [=> /[dup]] duplicates  *)
(* that hypothesis so that views can be applied more conveniently             *)
Goal (n < m) -> (n <= m) && ~~(m <= n).
Proof. by move=> /[dup] /ltnW -> /ltn_geF ->. Qed.

(** [move=> +.]*)
(* Doing [move=> /view] will only apply the view to the first hypothesis *)
(* of the goal. However, should we want to apply a view to a subsequent *)
(* goal hypothesis, the symbol + can be added for this purpose          *)
Goal (n < p) -> (p < m) -> (n <= m).
Proof.
  move=> /ltnW + /ltnW.
  by apply: leq_trans.
Qed.

(** Quickly instantiating a dependent type *)

Goal (forall x, P x) -> (P A -> Q A) -> Q A.
Proof.
  (* Eumbersome way of proving this: *)
  by move=> H; move: (H A)=> HA imp_PA_QA; move: HA.
  Restart.
  (* Elegant way of proving this *)
  move=> /(_ A).
  by move=> /[swap].
Qed.

(** Applying a function to a hypothesis *)
(* When having a hypothesis of the form [x = y], it is very useful if we can       *)
(* easily transform it into a hypothesis of the form [f x = f y] for some          *)
(* function [f]. This can be done with [move=> /(congr1 f)].                        *)
(* It is specially useful when we already have a hypotesis of the form [f x = f y] *)
(* with [g] being the left inverse of [f] and we want to get [x = y], as in the    *)
(* following case                                                                  *)
Goal succn n = succn m -> n = m.
Proof.
  move=> /(congr1 predn).
  by rewrite !succnK.
Qed.

(** Removing multiple constructor applications at once *)
(* Since constructors are injective by definition we can remove them when using an *)
(* intro pattern: [move=> [=]]. Notice that we can also rewrite with them and do    *)
(* everything we can usually do in an intro pattern                                *)
Goal S (S (S n)) = S (S (S m)) -> n = m.
Proof.
by move=> [=].
Restart.
move=> [= ->].
by exact: Logic.eq_refl.
Qed.


End implications.

(*** Equalities of booleans *)

Section bool_eqn.

(** Accessing data in records *)

(** [apply/val_eqP.] *)

(* When dealing with equalities of sigma types of the form                        *)
(*                                                                                *)
(* Record foo := {                                                                *)
(*   data : Type;                                                                 *)
(*   _ : proof_of_data data }.                                                    *)
(*                                                                                *)
(* if [foo1 foo2 : foo] have the same data but different proofs, Coq cannot       *)
(* compute a proof for equality automatically.                                    *)
(*                                                                                *)
(* The view [/val_eqP] reduces goals of the form [foo1 = foo2] to [data1 = data2] *)

Lemma lt_5_10 : 5 < 10. Proof. by []. Qed.

Definition num5 := Ordinal lt_5_10.

Definition num5' := @Ordinal 10 5 isT.

Definition num4 := @Ordinal 10 4 isT.

(* See that here Coq is capable of computing everything *)
Goal num4 < num5. Proof. by []. Qed.

(* Here, unless we use the view [/val_eqP], Coq cannot compute *)
Goal num5 = num5'.
Proof.
  (* See that Coq indeed cannot solve the goal by computation *)
  Fail by [].
  apply/val_eqP.
  by [].
Qed.

Variables n m : nat.

(** Conversioon boolean equations to implication of hypothesis *)
(* When proving the equality of boolean predicates, it is often more convenient    *)
(* to just proof that they are equivalent. [apply/idP/idP] transforms any          *)
(* equality between boolean predicates into the most convenient double implication *)
Goal (n < m) = ~~(m <= n).
Proof.
  apply/idP/idP.
Abort.

End bool_eqn.

(*** Sets and cardinalities *)

(* To be written *)
