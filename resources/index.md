---
# This file was generated from `meta.yml`, please do not edit manually.
# Follow the instructions on https://gitlab.com/ana-borges/templates to regenerate.
title: SSReflect tricks and treats
lang: en
header-includes:
  - |
    <style type="text/css"> body {font-family: Arial, Helvetica; margin-left: 5em; font-size: large;} </style>
    <style type="text/css"> h1 {margin-left: 0em; padding: 0px; text-align: center} </style>
    <style type="text/css"> h2 {margin-left: 0em; padding: 0px; color: #580909} </style>
    <style type="text/css"> h3 {margin-left: 1em; padding: 0px; color: #C05001;} </style>
    <style type="text/css"> body { width: 1100px; margin-left: 30px; }</style>
---

<div style="text-align:left"><img src="https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png" height="25" style="border:0px">
<a href="https://gitlab.com/JuanCoRo/ssreflect-tricks-and-treats">View the project on GitLab</a>
<img src="https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png" height="25" style="border:0px"></div>

## About

Welcome to the SSReflect tricks and treats project website!

This is a summary of all the nice tricks and treats I have been gathering about the [SSReflect](https://coq.inria.fr/refman/proof-engine/ssreflect-proof-language.html) proof language and the [MathComp](https://math-comp.github.io/) library in my years of proving.

This is an open source project, licensed under the Unlicense.

## Get the code

The current stable release of SSReflect tricks and treats can be [downloaded from GitLab](https://gitlab.com/JuanCoRo/ssreflect-tricks-and-treats/-/releases).

## Documentation

The coqdoc presentation of the source files can be browsed [here](ssreflect_t_and_t.ssrtt.html)

Related publications, if any, are listed below.


## Help and contact

- Report issues on [GitLab](https://gitlab.com/JuanCoRo/ssreflect-tricks-and-treats/issues)

## Authors and contributors

- Juan Conejero

